import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginToProceedPage } from './login-to-proceed';

@NgModule({
  declarations: [
    LoginToProceedPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginToProceedPage),
  ],
})
export class LoginToProceedPageModule {}
