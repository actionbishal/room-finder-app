import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the LoginToProceedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-to-proceed',
  templateUrl: 'login-to-proceed.html',
})
export class LoginToProceedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private modalCtrl:ModalController) {
  }

  openLoginPage() {
    let modal = this.modalCtrl.create('LoginPage');
    modal.present();
  }

}
