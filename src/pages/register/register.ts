import {Component, ViewChild} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {AppService} from"../../app/services/app.service";
import { Storage } from '@ionic/storage';
import {text} from "../../../node_modules/@angular/core/src/render3/instructions";
import {AccountPage} from "../account/account";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  @ViewChild('name') name;
  @ViewChild('email') email;
  @ViewChild('password') password;
  @ViewChild('confirm') confirm;
  private response: any;


  constructor(public navCtrl: NavController, public navParams: NavParams,
              public appService:AppService, private storage:Storage,
              public toastCtrl: ToastController, public loadingCtrl: LoadingController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }
  registerUser() {
    let loader = this.loadingCtrl.create({
      content: "Registering..."
    });
    loader.present();

    console.log('hello');
    if(this.password == this.confirm && this.email) {
      let post_data={
        name:this.name,
        email: this.email,
        password: this.password,
      };
      this.appService.registerUser(post_data).subscribe(response =>
      {
        this.response=response;
        this.storage.set('user_data', response);
        this.navCtrl.pop();

        this.presentToast('Register Successful...');
        loader.dismiss();
        // this.navCtrl.push(AccountPage)
      }, (err) => {
        console.log(err);
        this.presentToast('Registering Failed');
        loader.dismiss();


      });
    }
  }
}
