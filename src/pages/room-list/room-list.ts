import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AppService} from "../../app/services/app.service";
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
import {getResponseURL} from "../../../node_modules/@angular/http/src/http_utils";
import {RegisterPage} from "../register/register";
import {DetailsPage} from "../details/details";

/**
 * Generated class for the RoomListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-room-list',
  templateUrl: 'room-list.html',
})
export class RoomListPage {
  // public properties;
  properties:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http:Http) {
  }


  ionViewDidLoad() {
    // this.getProperties();
    console.log('ionViewDidLoad RoomListPage');

    this.http.get(' http://roomfinder.mountvisiontreks.com/api/get-properties').map(res => res.json()).subscribe(data => {
      this.properties = data;
      console.log(this.properties);
    });
  // getProperties() {
  //   this.appService.getProperties().subscribe(response => {
  //     this.properties = response;
  //     console.log(response);
  //     console.log("oops...!")
  //
  //   });
  }
  openDetailsPage(property){
    this.navCtrl.push(DetailsPage,{property:property});

  }

}
