import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";

@IonicPage()
@Component({
  selector: 'page-post-success',
  templateUrl: 'post-success.html',
})
export class PostSuccessPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  openHomePage() {
    this.navCtrl.setRoot(TabsPage);
  }

}
