import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostSuccessPage } from './post-success';

@NgModule({
  declarations: [
    PostSuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(PostSuccessPage),
  ],
})
export class PostSuccessPageModule {}
