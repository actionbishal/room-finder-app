import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import {AccountPage} from "../account/account";
import {FavoritePage} from "../favorite/favorite";
import {HomePage} from "../home/home";
import {ImageUploadPage} from "../property-post/image-upload/image-upload";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = FavoritePage ;
  tab3Root = AccountPage;

  constructor() {

  }
}
