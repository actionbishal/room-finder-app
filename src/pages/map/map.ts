import {Component, ElementRef, NgZone, ViewChild} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, Platform} from 'ionic-angular';
import {GoogleMapsProvider} from "../../providers/google-maps/google-maps";
import {LocationsProvider} from "../../providers/locations/locations";
import {ConnectivityServiceProvider} from "../../providers/connectivity-service/connectivity-service";
import {AppService} from "../../app/services/app.service";
import { Slides } from 'ionic-angular';
import {SingletonService} from "../../app/services/singleton.service";

/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare let google: any;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild(Slides) slides: Slides;
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  map: any;
  mapInitialised: boolean = false;
  properties: any;
  @ViewChild('locality') locality;
  longitude;
  latitude;
  searchLatLng;
  address;
  placesService: any;
  places: any = [];
  markers: any = [];
  city;
  baseUrl;
  apiKey: string = "AIzaSyCXeTAMlYENSsyPflrdJeAS-bK1eSf8O58";

  constructor(public navCtrl: NavController, public modalCtrl: ModalController,
              public singletonService:SingletonService,
              public zone: NgZone, public appService: AppService, public connectivityService: ConnectivityServiceProvider, public navParams: NavParams, public maps: GoogleMapsProvider, public platform: Platform, public locations: LocationsProvider) {
    this.properties = this.navParams.get('params').properties;
    // // console.log(this.properties);
    this.baseUrl=singletonService.baseUrl;

    this.searchLatLng = this.navParams.get('params').searchLatLng;
    this.city = this.appService.city;
  }

  ngAfterViewInit(): void {

    this.platform.ready().then(() => {
      this.locality = this.navParams.get('params').locality;

      let mapLoaded = this.init(this.mapElement.nativeElement, this.pleaseConnect).then(() => {
        if (this.mapInitialised) {
          this.properties.forEach((property,index) =>{
            // google.maps.Marker({position:{lat:property.latitude, lng:property.longitude}});
            // let mapOptions = {
            //   zoom: 15,
            //   disableDefaultUI: true
            // };
            this.markers = [];
            // this.map = new google.maps.Map(this.mapElement, mapOptions);
            let latLng = {lat: parseFloat(property.latitude), lng: parseFloat(property.longitude)};
            window.setTimeout(() => {
              let marker =new google.maps.Marker({
                position: latLng,
                map: this.map,
                title: 'Hello World!',
                // animation: google.maps.Animation.BOUNCE,
                icon: {
                  path: google.maps.SymbolPath.CIRCLE,
                  fillColor: '#f58c8c',
                  fillOpacity: 0.9,
                  strokeColor: '#fff',
                  strokeOpacity: 0.9,
                  strokeWeight: 1,
                  scale: 8
                }
              });
              this.markers.push(marker);
              google.maps.event.addListener(marker, 'click', () =>{
                console.log('wingardium leviosa');
                console.log(index);
                this.slideScroll(index);
                marker.setIcon({path:google.maps.SymbolPath.CIRCLE,
                  fillColor: '#aaa',
                  fillOpacity: 0.9,
                  strokeColor: '#fff',
                  strokeOpacity: 0.9,
                  strokeWeight: 1,
                  scale: 8})
              });
            }, 300);

          });          // this.populate();
          // this.map.addListener('center_changed', () => {
          //   let geoCoder = new google.maps.Geocoder;
          //   let latLng = {lat: this.map.getCenter().lat(), lng: this.map.getCenter().lng()};
          //   this.placesService.search({location: latLng, radius: 300}, (places) => {
          //     this.zone.run(() => {
          //       if (places) {
          //
          //         this.nearByPlaces = [];
          //         let centerLatLng = {lat: this.map.getCenter().lat(), lng: this.map.getCenter().lng()};
          //
          //         places.forEach((place) => {
          //           let placeLatLng = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
          //           // this.city = this.extractFromAdress(place.address_components, "locality");
          //
          //
          //           place.distance = LocationsProvider.haversineDistance(this.map.getCenter().lat(), this.map.getCenter().lng(),
          //             place.geometry.location.lat(), place.geometry.location.lng());
          //
          //           this.nearByPlaces.push(place);
          //         });
          //         const closestPlace = this.nearByPlaces.reduce(
          //           (locationA, locationB) =>
          //             locationA.distance < locationB.distance
          //               ? locationA
          //               : locationB
          //         );
          //         // this.getDetails({placeId: place.place_id}, (details) => {
          //         //
          //         //   this.zone.run(() => {
          //         //     this.locality = details.name;
          //         //     console.log(details);
          //         //     location.lat = details.geometry.location.lat();
          //         //     location.lng = details.geometry.location.lng();
          //         //     this.searchLatLng = {lat: location.lat, lng: location.lng};
          //         //     this.city = this.extractFromAdress(details.address_components, "locality");
          //         //     console.log(this.city);
          //         //     // let geoCoder = new google.maps.Geocoder;
          //         //     // this.zone.run(() => {
          //         //     //   geoCoder.geocode({'location': latLng}, (results, status) => {
          //         //     //     if (results && results[0]) {
          //         //     //       this.geocode = results[0].formatted_address;
          //         //     //       console.log(this.geocode);
          //         //     //     }
          //         //     //   });
          //         //     // });
          //         //   });
          //         // });
          //         console.log(closestPlace);
          //         this.address = closestPlace.vicinity;
          //       }
          //     });
          //   });
          // });


        }
      });

    });

  }

  private slideScroll(index) {

    this.slides.slideTo(index,300);
  }

  extractFromAdress(components, type) {
    for (let i = 0; i < components.length; i++)
      for (let j = 0; j < components[i].types.length; j++)
        if (components[i].types[j] == type) return components[i].long_name;
    return "";
  }

  init(mapElement: any, pleaseConnect: any): Promise<any> {

    this.mapElement = mapElement;
    this.pleaseConnect = pleaseConnect;

    return this.loadGoogleMaps();

  }

  loadGoogleMaps(): Promise<any> {

    return new Promise((resolve) => {

      if (typeof google == "undefined" || typeof google.maps == "undefined") {

        console.log("Google maps JavaScript needs to be loaded.");
        this.disableMap();

        if (this.connectivityService.isOnline()) {

          window['mapInit'] = () => {

            this.initMap().then(() => {
              resolve(true);
            });

            this.enableMap();
          };

          let script = document.createElement("script");
          script.id = "googleMaps";

          if (this.apiKey) {
            script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit&libraries=places';
          } else {
            script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
          }

          document.body.appendChild(script);

        }
      } else {

        if (this.connectivityService.isOnline()) {
          this.initMap();
          this.enableMap();
        }
        else {
          this.disableMap();
        }

        resolve(true);

      }

      this.addConnectivityListeners();

    });

  }

  initMap(): Promise<any> {

    this.mapInitialised = true;

    return new Promise((resolve) => {

      // this.geolocation.getCurrentPosition().then((position) => {

      let latLng = new google.maps.LatLng(this.searchLatLng.lat, this.searchLatLng.lng);
      let mapOptions = {
        center: latLng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        styles: [
          {
            featureType: "poi.business",
            elementType: "labels",
            stylers: [
              {visibility: "off"}
            ]
          },
          {
            featureType: "transit.station",
            elementType: "labels",
            stylers: [
              {visibility: "off"}
            ]
          },
          {
            featureType: "poi.park",
            elementType: "labels",
            stylers: [
              {visibility: "off"}
            ]
          }
        ]
      };
      this.map = new google.maps.Map(this.mapElement, mapOptions);
      window.setTimeout(() => {
        new google.maps.Marker({
          position: latLng,
          map: this.map,
          title: 'Hello World!',
          // animation: google.maps.Animation.BOUNCE,
          icon: {
            path: google.maps.SymbolPath.CIRCLE,
            fillColor: '#74cbff',
            fillOpacity: 0.9,
            strokeColor: '#fff',
            strokeOpacity: 0.9,
            strokeWeight: 1,
            scale: 8
          }
        })
      }, 300);

      resolve(true);
    });

    // });

  }

  disableMap(): void {
    if (this.pleaseConnect) {
      this.pleaseConnect.nativeElement.style.display = "block";
    }

  }

  enableMap(): void {

    if (this.pleaseConnect) {
      this.pleaseConnect.nativeElement.style.display = "none";
    }
  }


  addConnectivityListeners(): void {

    this.connectivityService.watchOnline().subscribe(() => {

      setTimeout(() => {

        if (typeof google == "undefined" || typeof google.maps == "undefined") {
          this.loadGoogleMaps();
        }
        else {
          if (!this.mapInitialised) {
            this.initMap();
          }

          this.enableMap();
        }

      }, 2000);

    });

    this.connectivityService.watchOffline().subscribe(() => {

      this.disableMap();

    });

  }

  // private populate() {
  //   let geoCoder = new google.maps.Geocoder();
  //   geoCoder.geocode({'address': this.city}, (results, status) =>{
  //     if (status === 'OK' && results) {
  //       this.map.setCenter(results[0].geometry.location);
  //       let post_data = {
  //         city:this.appService.city,
  //         // type:this.type
  //       };
  //       this.appService.getFilteredProperties(post_data).subscribe(response =>
  //       {
  //         this.properties=response;
  //         // this.storage.set('properties', response);
  //         console.log(response);
  //         for(let property of this.properties){
  //           // google.maps.Marker({position:{lat:property.latitude, lng:property.longitude}});
  //           // let mapOptions = {
  //           //   zoom: 15,
  //           //   disableDefaultUI: true
  //           // };
  //           this.markers=[];
  //           // this.map = new google.maps.Map(this.mapElement, mapOptions);
  //           let latLng = {lat:parseFloat(property.latitude), lng:parseFloat(property.longitude)};
  //           window.setTimeout(()=> {
  //             this.markers.push( new google.maps.Marker({
  //               position: latLng,
  //               map: this.map,
  //               title: 'Hello World!',
  //               // animation: google.maps.Animation.BOUNCE,
  //               icon: {
  //                 path: google.maps.SymbolPath.CIRCLE,
  //                 fillColor: '#f58c8c',
  //                 fillOpacity: 0.9,
  //                 strokeColor: '#fff',
  //                 strokeOpacity: 0.9,
  //                 strokeWeight: 1,
  //                 scale: 8
  //               }
  //             }))
  //           },300);
  //         }
  //       });
  //     } else {
  //       alert('Geocode was not successful for the following reason: ' + status);
  //     }
  //   });
  // }

  openSearchPage() {
    let modal = this.modalCtrl.create('SearchPage');
    modal.present();
  }
  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    let property  =this.properties[currentIndex];
    if(property) {
      this.map.panTo({lat: parseFloat(property.latitude),lng:parseFloat(property.longitude)});
      // let latLng = {lat: parseFloat(property.latitude), lng: parseFloat(property.longitude)};
      // console.log(this.markers);
    }
  }
  openDetailsPage(property){
    this.navCtrl.push('DetailsPage',{property:property});
  }

}
