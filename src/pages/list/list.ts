import {Component, ViewChild} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {AppService} from "../../app/services/app.service";
import {SingletonService} from "../../app/services/singleton.service";

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {
  @ViewChild('locality') locality;
  properties:any;
  baseUrl;
  searchLatLng;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl : ModalController, public appService: AppService,public singletonService:SingletonService) {
    this.baseUrl = this.singletonService.baseUrl;
    this.properties = this.navParams.get('params').properties;
    this.searchLatLng = this.navParams.get('params').searchLatLng;
  }
  ngOnInit() {
    this.locality = this.navParams.get('params').locality;
  }
  openSearchPage() {
    let modal = this.modalCtrl.create('SearchPage');
    modal.present();
  }
  openDetailsPage(property){
    this.navCtrl.push('DetailsPage',{property:property,locality:this.locality});
  }
}
