import {Component} from '@angular/core';
import {
  IonicPage,

  NavController,
  NavParams,
  Platform,
} from 'ionic-angular';
import {AppService} from "../../app/services/app.service";
import {SingletonService} from "../../app/services/singleton.service";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the MyListingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-listings',
  templateUrl: 'my-listings.html',
})
export class MyListingsPage {

  baseUrl: string;
  properties: any;
  user_data;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public appService: AppService,
              public singletonService: SingletonService,
              public storage: Storage) {

    this.baseUrl = singletonService.baseUrl;
    this.user_data = this.appService.user_data;
    this.appService.userProperty().subscribe(response => {
      this.properties = response;
      console.log(this.properties);
    });
  }
}


