import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {TabsPage} from "../tabs/tabs";
import {AppService} from "../../app/services/app.service";

/**
 * Generated class for the SelectCityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-city',
  templateUrl: 'select-city.html',
})
export class SelectCityPage {

  city = 'Kathmandu';
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage:Storage,public appService :AppService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectCityPage');
  }
  onCitySelected() {
    this.storage.set('city', this.city);
    this.appService.city = this.city;
    this.navCtrl.setRoot(TabsPage);
  }
}
