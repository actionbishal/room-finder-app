import {Component, ViewChild} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {RegisterPage} from "../register/register";
import {BeginPostingPage} from "../property-post/begin-posting/begin-posting";
import {AppService} from "../../app/services/app.service";
import { Storage } from '@ionic/storage';
import {HomePage} from "../home/home";
import {AccountPage} from "../account/account";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @ViewChild('email') email;
  @ViewChild('password') password;
  private user_data:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public appService: AppService,
              private storage:Storage,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController) {
  }

  openBeginPostingPage(){
    this.navCtrl.push(BeginPostingPage);
  }
  openRegisterPage(){
    this.navCtrl.pop();
    this.navCtrl.push('RegisterPage');
  }
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1000,
      position: 'bottom'
    });
    toast.present();
  }
  loginUser() {

    let loader = this.loadingCtrl.create({
      content: "Logging..."
    });
    loader.present();

    let credentials = {
      email: this.email,
      password:this.password
    };
    if(this.email && this.password) {
      this.appService.login(credentials).subscribe(response => {
        this.user_data = response;
        this.storage.set('user_data', response);
        this.navCtrl.pop();

        this.presentToast('Login Successful...');
            loader.dismiss();
          }, (err) => {
            console.log(err);
            this.presentToast('Login Failed');
            loader.dismiss();
      });
    }
  }
}
