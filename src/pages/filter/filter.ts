import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {
  type='rent';
  price = {lower: 0,steps:1000, upper: 50000};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  filterProperties() {}

}
