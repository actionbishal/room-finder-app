import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AppService} from "../../app/services/app.service";
import {SingletonService} from "../../app/services/singleton.service";

/**
 * Generated class for the FavoritePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html',
})
export class FavoritePage {

  properties;
  baseUrl;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public appService: AppService,
              public singletonService:SingletonService

  ) {
    this.baseUrl=singletonService.baseUrl;
  }

  ionViewWillEnter() {
    this.appService.getFavoriteProperty().subscribe((response)=>{
      this.properties = response;
      console.log(response);
    })
  }

  openDetailsPage(property){
    this.navCtrl.push('DetailsPage',{property:property});
  }
}
