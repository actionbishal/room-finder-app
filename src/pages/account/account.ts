import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {HomePage} from "../home/home";
import {TabsPage} from "../tabs/tabs";
import {AppService} from "../../app/services/app.service";

/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  public user_data :any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage:Storage,
              public toastCtrl: ToastController, public appService : AppService) {
  }

  ionViewWillEnter() {
    this.storage.get('user_data').then((val) => {
      this.user_data = val;
    });
  }

  openAboutPage() {
    this.navCtrl.push('AboutPage');
  }
  openLoginPage(){
    this.navCtrl.push('LoginPage');
  }
  openMyListingsPage() {

    this.navCtrl.push('MyListingsPage');

  }
  openProfilePage() {
    this.navCtrl.push('ProfilePage');
  }

  logout(){
    this.storage.set('user_data',"");
    this.appService.user_data = '';
    this.user_data = '';
    this.presentToast("logged out");
  }
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1000,
      position: 'bottom'
    });
    toast.present();
  }

}
