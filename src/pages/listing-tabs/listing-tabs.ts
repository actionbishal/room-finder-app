import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ListPage} from "../list/list";
import {MapPage} from "../map/map";

/**
 * Generated class for the ListingTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listing-tabs',
  templateUrl: 'listing-tabs.html',
})
export class ListingTabsPage {

  tab1Root: any = ListPage;
  tab2Root: any = MapPage;
  params;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.params = this.navParams.data;
    console.log(this.params);
  }
}
