import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListingTabsPage } from './listing-tabs';

@NgModule({
  declarations: [
    ListingTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(ListingTabsPage),
  ],
})
export class ListingTabsPageModule {}
