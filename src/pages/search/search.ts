import {Component, ElementRef, NgZone, ViewChild} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, Platform} from 'ionic-angular';
import {ListingTabsPage} from "../listing-tabs/listing-tabs";
import {ConnectivityServiceProvider} from "../../providers/connectivity-service/connectivity-service";
import {AppService} from "../../app/services/app.service";
import { Geolocation } from '@ionic-native/geolocation';
import {LocationsProvider} from "../../providers/locations/locations";

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare let google: any;

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  @ViewChild('locality') locality;
  places: any = [];
  properties;
  searchDisabled: boolean;
  searchLatLng;
  latitude: number;
  longitude: number;
  city: any;
  type;
  propertiesWithDistance: any = [];
  nearByPlaces:any= [];
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  apiKey: string = "AIzaSyCXeTAMlYENSsyPflrdJeAS-bK1eSf8O58";

  constructor(public navCtrl: NavController, public zone: NgZone, public navParams: NavParams,
              public connectivityService: ConnectivityServiceProvider,
              public appService:AppService,
              public geolocation: Geolocation,
              public loadingCtrl: LoadingController,
              public platform: Platform) {
    this.searchDisabled = true;
    this.type = "rent";
    // this.searchControl = new FormControl();
    // this.price.lower=5000;
  }
  ngAfterViewInit(): void {
    this.platform.ready().then(() => {
      let mapLoaded = this.loadGoogleMaps()
    });
  }

  loadGoogleMaps(): Promise<any> {

    return new Promise((resolve) => {

      if (typeof google == "undefined" || typeof google.maps == "undefined") {

        console.log("Google maps JavaScript needs to be loaded.");
        if (this.connectivityService.isOnline()) {

          console.log('online');
          let script = document.createElement("script");
          script.id = "googleMaps";

          if (this.apiKey) {
            script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&libraries=places&region=np';
          } else {
            script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
          }

          document.body.appendChild(script);
          this.enableMap();
        }
      } else {

        if (this.connectivityService.isOnline()) {
          // this.loadGoogleMaps();
          this.enableMap();
        }
        resolve(true);

      }

      this.addConnectivityListeners();

    });

  }

  openListingTabsPage() {
    this.navCtrl.push(ListingTabsPage);
  }

  selectPlace(place) {

    this.places = [];

    let location = {
      lat: null,
      lng: null,
      name: place.name
    };

    new google.maps.places.PlacesService(document.createElement('div')).getDetails({placeId: place.place_id}, (details) => {

      this.zone.run(() => {
        this.locality = details.name;
        console.log(details);
        location.lat = details.geometry.location.lat();
        location.lng = details.geometry.location.lng();
        this.searchLatLng = {lat: location.lat, lng: location.lng};
        this.city = this.extractFromAdress(details.address_components, "locality");
        this.filterProperties();
        console.log(this.city);
        // let geoCoder = new google.maps.Geocoder;
        // this.zone.run(() => {
        //   geoCoder.geocode({'location': latLng}, (results, status) => {
        //     if (results && results[0]) {
        //       this.geocode = results[0].formatted_address;
        //       console.log(this.geocode);
        //     }
        //   });
        // });
      });
    });
  }
  extractFromAdress(components, type){
    for (let i=0; i<components.length; i++)
      for (let j=0; j<components[i].types.length; j++)
        if (components[i].types[j]==type) return components[i].long_name;
    return "";
  }
  searchPlace() {

    if (this.locality.length > 0) {

      let config = {
        componentRestrictions: {country: 'np'},
        input: this.locality,
        language:'en'
      };


      new google.maps.places.AutocompleteService().getPlacePredictions(config, (predictions, status) => {
        if (status == google.maps.places.PlacesServiceStatus.OK && predictions) {

          this.places = [];

          predictions.forEach((prediction) => {
            this.places.push(prediction);
          });
        }

      });

    } else {
      this.places = [];
    }

  }


  addConnectivityListeners(): void {

    this.connectivityService.watchOnline().subscribe(() => {

      setTimeout(() => {

        if (typeof google == "undefined" || typeof google.maps == "undefined") {
          this.loadGoogleMaps();
        }
        else {
          this.enableMap();
        }

      }, 2000);

    });

    this.connectivityService.watchOffline().subscribe(() => {

      this.disableMap();

    });

  }

  disableMap(): void {

    if (this.pleaseConnect) {
      this.pleaseConnect.nativeElement.style.display = "block";
    }

  }

  enableMap(): void {
    console.log('enabled');

    if (this.pleaseConnect) {
      this.pleaseConnect.nativeElement.style.display = "none";
    }

  }

  filterProperties() {
    let post_data = {
      city:this.city,
      type:this.type
    };

    this.appService.getFilteredProperties(post_data).subscribe(response =>
    {
      this.properties=response;
      // this.storage.set('properties', response);
      console.log(response);
      this.propertiesWithDistance = [];
      this.properties.forEach((property,index) =>{
        property.distance = LocationsProvider.haversineDistance(this.searchLatLng.lat, this.searchLatLng.lng,property.latitude,property.longitude).toFixed(2);
        this.propertiesWithDistance.push(property);
      });
      this.propertiesWithDistance.sort((locationA, locationB) => {
        return locationA.distance - locationB.distance;
      });
      let params = {
        properties :this.propertiesWithDistance,
        searchLatLng:this.searchLatLng,
        locality:this.locality
      };
      console.log(params);

      this.navCtrl.push(ListingTabsPage, {params: params});
    });
  }
  setNearBy() {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    loader.present();
    this.geolocation.getCurrentPosition().then((resp) => {
      if(resp) {
        loader.dismiss();
        this.searchLatLng = {lat: resp.coords.latitude, lng: resp.coords.longitude};
        new google.maps.places.PlacesService(document.createElement('div')).search({location: this.searchLatLng, radius: 300}, (places) => {
          this.zone.run(() => {
            if (places) {

              this.nearByPlaces = [];

              places.forEach((place) => {
                place.distance = LocationsProvider.haversineDistance(resp.coords.latitude, resp.coords.longitude,
                  place.geometry.location.lat(), place.geometry.location.lng());

                this.nearByPlaces.push(place);
              });
              const closestPlace = this.nearByPlaces.reduce(
                (locationA, locationB) =>
                  locationA.distance < locationB.distance
                    ? locationA
                    : locationB
              );
              console.log(closestPlace);
              this.locality = closestPlace.vicinity;
            }
          });
        });

        this.filterProperties();
      }

    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

}
