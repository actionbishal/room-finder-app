import { Component } from '@angular/core';
import {
  ActionSheetController,
  IonicPage,
  Loading, LoadingController,
  NavController,
  NavParams,
  Platform,
  ToastController
} from 'ionic-angular';
import {AppService} from "../../app/services/app.service";
import { Camera, CameraOptions } from '@ionic-native/camera'
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {File} from "@ionic-native/file";
import {FilePath} from "@ionic-native/file-path";
import {SingletonService} from "../../app/services/singleton.service";
import {ProfileDataPage} from "../profile-data/profile-data";
import { Storage } from '@ionic/storage';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  lastImage: string = null;
  myphoto: any = 'assets/imgs/default_image.png';
  loading: Loading;
  baseUrl: string;
  property: any;
  user_data;
  // user_data = {
  //   name:'',
  //   phone:'',
  //   email:'',
  //   profile: {
  //     phone:''
  //   }
  // };


  constructor(public navCtrl: NavController, public navParams: NavParams,
              public appService: AppService,
              private camera: Camera,
              private transfer: FileTransfer, private file: File, private filePath: FilePath,
              public actionSheetCtrl: ActionSheetController,
              public toastCtrl: ToastController, public platform: Platform,
              public loadingCtrl: LoadingController,
              public singletonService: SingletonService,
              public storage:Storage) {

    this.baseUrl = singletonService.baseUrl + 'api/';
    this.user_data = this.appService.user_data;
  }

  ngOnInit() {

  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.cropImage();
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePhoto();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:

      this.myphoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  cropImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit: true,
      targetWidth: 300,
      targetHeight: 300
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  uploadImage() {
    //Show loading

    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    //create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();

    //random int

    var random = Math.floor(Math.random() * 100);
    let fileName = "myImage_" + random + ".jpg";
    //option transfer
    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: "property_img_" + random + ".jpg",
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: "image/jpeg",
      // mimeType: "multipart/form-data",
      params: {'fileName': fileName, 'property': this.property.id},
      headers: {}
    }

    //file transfer action
    fileTransfer.upload(this.myphoto, this.baseUrl + 'property/image/upload', options)
      .then((data) => {
        this.presentToast('Image succesfully uploaded.');
        loader.dismiss();
      }, (err) => {
        console.log(err);
        this.presentToast('Cannot Upload Image.');
        loader.dismiss();
      });

  }

  openProfileDataPage() {
    this.navCtrl.push('ProfileDataPage',{user_data:this.user_data});
  }
}
  // this.storage.get('address').then((val) => {
  //   this.address = val;
  // });

