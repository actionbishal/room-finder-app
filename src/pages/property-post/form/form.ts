import {Component, ViewChild} from '@angular/core';
import {
  IonicPage, LoadingController, NavController, NavParams, ToastController
} from 'ionic-angular';
import {AppService} from "../../../app/services/app.service";
/**
 * Generated class for the FormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form',
  templateUrl: 'form.html',
})
  export class FormPage {
  @ViewChild('name') name;
  @ViewChild('description') description;
  @ViewChild('price') price;
  furnishing = "Non Furnished";
  @ViewChild('parking') parking;
  preferredTenants = 'Anyone';
  @ViewChild('locality') locality;
  negotiable = 0;
  @ViewChild('contactnumber') contactnumber;

  private property:any;

  constructor(public navCtrl: NavController, public navParams: NavParams ,
              public appService:AppService, public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
    this.property = this.navParams.get('property');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormPage');
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

  submitProperty() {
    if(this.contactnumber && this.name && this.price) {
      let loader = this.loadingCtrl.create({
        content: "posting..."
      });
      loader.present();

      let post_data={
        name:this.name,
        description: this.description,
        preferredTenants:this.preferredTenants,
        furnishing:this.furnishing,
        locality:this.locality,
        price: this.price,
        parking:this.parking,
        negotiable:this.negotiable,
        contact:this.contactnumber
      };


      this.appService.updateProperty(this.property,post_data).subscribe(response =>
      {
        this.presentToast('Description added.');
        loader.dismiss();
        this.property=response;
        this.navCtrl.push('ImageUploadPage',{property:this.property});

      }, (err) => {
        console.log(err);
        this.presentToast('Failed!!!');
        loader.dismiss();


      });
    } else {
      this.presentToast("Please fill all the required fields.");
    }


  }
}

