import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {AppService} from "../../../app/services/app.service";

/**
 * Generated class for the BeginPostingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-begin-posting',
  templateUrl: 'begin-posting.html',
})
export class BeginPostingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,  public modalCtrl: ModalController, public appService: AppService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BeginPostingPage');
  }
  openFormPage(){
    if(this.appService.user_data) {
      let modal = this.modalCtrl.create('TypeAndCityPage');
      modal.present();
      // this.navCtrl.push('FormPage');
    } else {
      let modal = this.modalCtrl.create('LoginToProceedPage');
      modal.present()
    }
  }
  openHomePage(){
    this.navCtrl.push('HomePage');
  }

}
