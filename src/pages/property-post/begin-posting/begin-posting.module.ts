import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeginPostingPage } from './begin-posting';

@NgModule({
  declarations: [
    BeginPostingPage,
  ],
  imports: [
    IonicPageModule.forChild(BeginPostingPage),
  ],
})
export class BeginPostingPageModule {}
