import {IonicPage, NavController, Platform, ViewController} from 'ionic-angular';
import { Component, ElementRef, ViewChild, NgZone } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import {GoogleMapsProvider} from "../../../providers/google-maps/google-maps";

declare let google: any;

@IonicPage()
@Component({
  selector: 'page-location-select',
  templateUrl: 'location-select.html'
})
export class LocationSelectPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  latitude: number;
  longitude: number;
  autocompleteService: any;
  placesService: any;
  @ViewChild('query') query;
  places: any = [];
  searchDisabled: boolean;
  saveDisabled: boolean;
  location: any;

  constructor(public navCtrl: NavController, public zone: NgZone, public maps: GoogleMapsProvider, public platform: Platform, public geolocation: Geolocation, public viewCtrl: ViewController) {
    this.searchDisabled = true;
    this.saveDisabled = true;
  }


  ngAfterViewInit(): void {
    this.platform.ready().then(() => {

        let mapLoaded = this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement).then(() => {
          this.autocompleteService = new google.maps.places.AutocompleteService();
          this.placesService = new google.maps.places.PlacesService(this.maps.map);
          this.searchDisabled = false;

          this.maps.map.addListener('center_changed', () =>{
            // let geoCoder = new google.maps.Geocoder;
            let latLng = {lat: this.maps.map.getCenter().lat(), lng: this.maps.map.getCenter().lng()};
            // this.zone.run(() => {
            // // geoCoder.geocode({'location': latLng}, (results, status) => {
            // //   if (results) {
            // //     this.query = results[0].formatted_address;
            // //     console.log(this.query);
            // //   }
            // // });
              this.placesService.search({location: latLng,radius:300}, (details) => {
                this.zone.run(() => {
                  console.log(details);
                  console.log(details[0].geometry.location.lat());
                  // if(details[1]) {
                  //   this.query = details[1].name;
                  // }
                });
              });

            });
      });

    });

  }

  selectPlace(place){

    this.places = [];

    let location = {
      lat: null,
      lng: null,
      name: place.name
    };

    this.placesService.getDetails({placeId: place.place_id}, (details) => {

      this.zone.run(() => {
        this.query = details.name;
        location.name = details.name;
        location.lat = details.geometry.location.lat();
        location.lng = details.geometry.location.lng();
        this.saveDisabled = false;
        this.maps.map.setCenter({lat: location.lat, lng: location.lng});
        this.location = location;
      });
    });

  }

  searchPlace(){

    this.saveDisabled = true;

    if(this.query.length > 0 && !this.searchDisabled) {

      let config = {
        types: ['geocode'],
        input: this.query
      };

      this.autocompleteService.getPlacePredictions(config, (predictions, status) => {
        if(status == google.maps.places.PlacesServiceStatus.OK && predictions){

          this.places = [];

          predictions.forEach((prediction) => {
            this.places.push(prediction);
          });
        }

      });

    } else {
      this.places = [];
    }

  }

  save(){
    this.viewCtrl.dismiss(this.location);
  }

  close(){
    this.viewCtrl.dismiss();
  }

}
