import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TypeAndCityPage } from './type-and-city';

@NgModule({
  declarations: [
    TypeAndCityPage,
  ],
  imports: [
    IonicPageModule.forChild(TypeAndCityPage),
  ],
})
export class TypeAndCityPageModule {}
