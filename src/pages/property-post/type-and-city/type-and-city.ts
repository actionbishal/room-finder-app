import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {AppService} from "../../../app/services/app.service";

/**
 * Generated class for the TypeAndCityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-type-and-city',
  templateUrl: 'type-and-city.html',
})
export class TypeAndCityPage {

  type = 'rent';
  city = 'Kathmandu';
  property:any;
  loading;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private appService: AppService, public toastCtrl: ToastController,
              public loadingCtrl: LoadingController) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad TypeAndCityPage');
  }
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 4000,
      position: 'bottom'
    });
    toast.present();
  }

  submitProperty() {
    let loader = this.loadingCtrl.create({
      content: "Please Wait..."
    });
    loader.present();

    let post_data={
      type:this.type,
      city: this.city,
    };
    this.appService.submitProperty(post_data).subscribe(response =>
    {
      this.property=response;
      console.log(this.property);
      this.navCtrl.push('LocalityAddressPage',{property:this.property});

      this.presentToast('Proceed now...');
      loader.dismiss();
    }, (err) => {
      console.log(err);
      this.presentToast('Failed to proceed');
      loader.dismiss();



    });
  }

}
