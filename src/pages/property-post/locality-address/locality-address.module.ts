import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalityAddressPage } from './locality-address';

@NgModule({
  declarations: [
    LocalityAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(LocalityAddressPage),
  ],
})
export class LocalityAddressPageModule {}
