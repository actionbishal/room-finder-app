import {Component, ElementRef, NgZone, ViewChild} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, Platform, ToastController} from 'ionic-angular';
import {GoogleMapsProvider} from "../../../providers/google-maps/google-maps";
import {AppService} from "../../../app/services/app.service";
import {LocationsProvider} from "../../../providers/locations/locations";

/**
 * Generated class for the LocalityAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare let google: any;

@IonicPage()
@Component({
  selector: 'page-locality-address',
  templateUrl: 'locality-address.html',
})
export class LocalityAddressPage {
  @ViewChild('locality') locality;
  @ViewChild('address') address;
  @ViewChild('landmark') landmark;
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  // locality;
  latitude: number;
  longitude: number;
  geocode: any;
  autocompleteService: any;
  placesService: any;
  places: any = [];
  nearByPlaces: any = [];
  searchDisabled: boolean;
  saveDisabled: boolean;
  location: any;
  private property: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public zone: NgZone,
              public maps: GoogleMapsProvider,
              private appService: AppService,
              public platform: Platform,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController) {
    this.searchDisabled = true;
    this.saveDisabled = true;
    this.property = this.navParams.get('property');
  }

  ngAfterViewInit(): void {
    this.platform.ready().then(() => {

      let mapLoaded = this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement).then(() => {
        this.autocompleteService = new google.maps.places.AutocompleteService();
        this.placesService = new google.maps.places.PlacesService(this.maps.map);
        this.searchDisabled = false;

        this.maps.map.addListener('center_changed', () => {
          let geoCoder = new google.maps.Geocoder;
          let latLng = {lat: this.maps.map.getCenter().lat(), lng: this.maps.map.getCenter().lng()};
          this.zone.run(() => {
            geoCoder.geocode({'location': latLng}, (results, status) => {
              console.log(results);
              if (results && results[0]) {

                this.geocode = results[0].formatted_address;
                console.log(this.geocode);
                this.placesService.search({location: latLng, radius: 300}, (places) => {
                  this.zone.run(() => {
                    if (places) {

                      this.nearByPlaces = [];
                      let centerLatLng = {lat: this.maps.map.getCenter().lat(), lng: this.maps.map.getCenter().lng()};

                      places.forEach((place) => {
                        let placeLatLng = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};

                        place.distance = LocationsProvider.haversineDistance(this.maps.map.getCenter().lat(), this.maps.map.getCenter().lng(),
                          place.geometry.location.lat(), place.geometry.location.lng());

                        this.nearByPlaces.push(place);
                      });
                      const closestPlace = this.nearByPlaces.reduce(
                        (locationA, locationB) =>
                          locationA.distance < locationB.distance
                            ? locationA
                            : locationB
                      );
                      console.log(closestPlace);
                      this.address = closestPlace.vicinity;
                      this.locality = closestPlace.name;
                    }
                  });
                });
              }
            });
          });

        });
      });

    });

  }


  selectPlace(place) {

    this.places = [];

    let location = {
      lat: null,
      lng: null,
      name: place.name
    };

    this.placesService.getDetails({placeId: place.place_id}, (details) => {

      this.zone.run(() => {
        this.locality = details.name;
        location.name = details.name;
        location.lat = details.geometry.location.lat();
        location.lng = details.geometry.location.lng();
        this.saveDisabled = false;
        this.maps.map.setCenter({lat: location.lat, lng: location.lng});
        this.location = location;
      });
    });

  }

  searchPlace() {
    this.saveDisabled = true;

    if (this.locality.length > 0 && !this.searchDisabled) {

      let config = {
        componentRestrictions: {country: 'np'},
        input: this.locality
      };

      this.autocompleteService.getPlacePredictions(config, (predictions, status) => {
        if (status == google.maps.places.PlacesServiceStatus.OK && predictions) {

          this.places = [];

          predictions.forEach((prediction) => {
            this.places.push(prediction);
          });
        }

      });

    } else {
      this.places = [];
    }

  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

  submitProperty() {

    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    loader.present();

    let post_data = {
      locality: this.locality,
      address: this.address,
      landmark: this.landmark,
      latitude: this.maps.map.getCenter().lat(),
      longitude: this.maps.map.getCenter().lng(),
      geocode: this.geocode
    };

    console.log(this.property);
    this.appService.updatePropertyLocation(this.property, post_data).subscribe(response => {
      this.property = response;
      this.presentToast('Locality Added...');
      loader.dismiss();
      this.navCtrl.push('FormPage', {property: this.property});
    }, (err) => {
      console.log(err);
      this.presentToast('Location is not added!!!');
      loader.dismiss();


    });
  }
}
