import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
import get = Reflect.get;
import {SingletonService} from "../../app/services/singleton.service";
import {AppService} from "../../app/services/app.service";

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
  property: any;
  baseUrl:any;
  locality;
  constructor(public navCtrl: NavController,public http:Http,public params:NavParams,
              public singletonService:SingletonService, public appService: AppService,
              public toastCtrl:ToastController
  ) {
    this.property= params.get("property");
    this.baseUrl=singletonService.baseUrl;
    console.log(this.property);
  }
  ngAfterViewInit() {
    this.locality= this.params.get("locality");
  }
  addToFavorite(id) {
    this.presentToast("Added to favorites");
    let post_data = {
      user_id: this.appService.user_data.id,
      property_id: this.property.id
    };
    this.appService.addFavoriteProperty(post_data).subscribe(response => {
      console.log(response)
    })
  }
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();

  }
}
