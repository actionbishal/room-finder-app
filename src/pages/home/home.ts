import {Component, ElementRef, Renderer, ViewChild} from '@angular/core';
import {Content, ModalController, NavController, NavParams} from 'ionic-angular';
import {AppService} from "../../app/services/app.service";
import {RoomListPage} from "../room-list/room-list";
import {SingletonService} from "../../app/services/singleton.service";
import { Storage } from '@ionic/storage';
import {ListingTabsPage} from "../listing-tabs/listing-tabs";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private properties;
  showDemoBox:boolean;
  private searchRef;
  public index=0;
  baseUrl:string;
  private city:any="Select City";
  constructor(public navCtrl: NavController,
              public appService: AppService,
              public element:ElementRef,
              public renderer:Renderer,
              public modalCtrl: ModalController,
              public singletonService:SingletonService,
              public storage:Storage
              ) {
    this.showDemoBox=true;
    this.baseUrl=singletonService.baseUrl;
    storage.get('city').then((val) => {
      this.city = val;
    });
  }

  openRoomListPage() {
    this.navCtrl.push(RoomListPage);
  }

  openBeginPostingPage() {
    this.navCtrl.push('BeginPostingPage');

  }

  ionViewDidLoad() {
    if(this.city=="") {
      this.openSelectCityPage()
    }
    this.getRecentProperties();
  }

  getRecentProperties() {
    this.appService.getRecentProperties().subscribe(response => {
      this.properties = response;
    });
  }
  ngAfterViewInit() {
    this.searchRef = this.element.nativeElement.getElementsByClassName("search")[0];
    this.searchRef.getElementsByTagName("input").disabled=true;
  }
  handleScroll(event: Content) {
    if (event.scrollTop > 10) {
      this.renderer.setElementStyle(this.searchRef, 'display', 'block');
    }
    else{
      this.renderer.setElementStyle(this.searchRef, 'display', 'none');
    }
  }
  openSearchPage(){
    // this.navCtrl.push('SearchPage');
    let modal = this.modalCtrl.create('SearchPage');
    modal.present();
  }
  openSelectCityPage() {
    let modal = this.modalCtrl.create('SelectCityPage');
    modal.present();
  }
  openDetailsPage(property){
    this.navCtrl.push('DetailsPage',{property:property});
  }
}
