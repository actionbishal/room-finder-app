import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  IonicPage,
  Loading, LoadingController,
  NavController,
  NavParams,
  Platform,
  ToastController
} from 'ionic-angular';
import {AppService} from "../../app/services/app.service";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {File} from "@ionic-native/file";
import {FilePath} from "@ionic-native/file-path";
import {SingletonService} from "../../app/services/singleton.service";
import {Storage} from "@ionic/storage";
import {TabsPage} from "../tabs/tabs";
import {ProfilePage} from "../profile/profile";

/**
 * Generated class for the ProfileDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-data',
  templateUrl: 'profile-data.html',
})
export class ProfileDataPage {
  @ViewChild('fullname') name;
  @ViewChild('email') email;
  @ViewChild('address') address;
  @ViewChild('phonenumber') phonenumber;
  // private profile:any;
// fullname:string;
// email:string;

// key:string='username';
  lastImage: string = null;
  myphoto:any= 'assets/imgs/default_image.png';
  loading: Loading;
  baseUrl:string;
  property:any;
  user_data:any={
    name:'',
    email:'',
    profile: {
      phone: ''
    }}

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public appService:AppService,
              private camera: Camera,
              private transfer: FileTransfer, private file: File, private filePath: FilePath,
              public actionSheetCtrl: ActionSheetController,
              public toastCtrl: ToastController, public platform: Platform,
              public loadingCtrl: LoadingController,
              public singletonService:SingletonService,
              public storage:Storage) {

    this.baseUrl = singletonService.baseUrl+'api/';
    this.property = this.navParams.get("property")

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ImageUploadPage');
  }
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.cropImage();
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePhoto();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  takePhoto(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:

      this.myphoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  cropImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit:true,
      targetWidth:300,
      targetHeight:300
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  uploadImage() {
    //Show loading

    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    //create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();

    //random int

    var random = Math.floor(Math.random() * 100);
    let fileName = "myImage_" + random + ".jpg";
    //option transfer
    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: "property_img_" + random + ".jpg",
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: "image/jpeg",
      // mimeType: "multipart/form-data",
      params: {'fileName': fileName, 'property': this.property.id},
      headers: {}
    }

    //file transfer action
    fileTransfer.upload(this.myphoto, this.baseUrl + 'property/image/upload', options)
      .then((data) => {
        this.presentToast('Image succesfully uploaded.');
        loader.dismiss();
      }, (err) => {
        console.log(err);
        this.presentToast('Cannot Upload Image.');
        loader.dismiss();
      });

  }
  openProfileDataPage(){
    this.navCtrl.push(ProfileDataPage);
  }
  onSaveChange(){
    this.storage.set('name',this.name) .then((value) =>{
      console.log('name',value);});
      this.storage.set('email',this.email).then((value) =>{
      console.log('email',value);});
      this.storage.set('address',this.address).then((value) =>{
      console.log('address',value);});
      this.storage.set('phonenumber',this.phonenumber)
      .then((value) =>{
      console.log('number',value);

      this.navCtrl.setRoot(ProfilePage);
    } )
  }

}

