import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AccountPage} from "../pages/account/account";
import {HomePage} from "../pages/home/home";
import { AboutPage } from '../pages/about/about';
import {FavoritePage} from "../pages/favorite/favorite";
import {AppService} from "./services/app.service";
import {SingletonService} from "./services/singleton.service";
import {HttpModule} from "@angular/http";
import {RoomListPage} from "../pages/room-list/room-list";
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import {FileTransfer} from "@ionic-native/file-transfer";
import {IonicStorageModule} from "@ionic/storage";
import { ConnectivityServiceProvider } from '../providers/connectivity-service/connectivity-service';
import { GoogleMapsProvider } from '../providers/google-maps/google-maps';
import { Network } from '@ionic-native/network';
import { Geolocation } from '@ionic-native/geolocation';
import {MapPage} from "../pages/map/map";
import {ListPage} from "../pages/list/list";
import {ListingTabsPage} from "../pages/listing-tabs/listing-tabs";
import { LocationsProvider } from '../providers/locations/locations';
import {MyListingsPage} from "../pages/my-listings/my-listings";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AboutPage,
    FavoritePage,
    RoomListPage,
    AccountPage,
    TabsPage,
    MapPage,
    ListPage,
    ListingTabsPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false,
      tabsHideOnSubPages: true,
    }),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AboutPage,
    FavoritePage,
    RoomListPage,
    AccountPage,
    TabsPage,
    MapPage,
    ListPage,
    ListingTabsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AppService,
    SingletonService,
    File,
    FileTransfer,
    Camera,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConnectivityServiceProvider,
    GoogleMapsProvider,
    Network,
    Geolocation,
    LocationsProvider,
  ]
})
export class AppModule {}
