import { Injectable } from '@angular/core';
import {Http} from "@angular/http";

@Injectable()
export class SingletonService {
  http:any;
  searching:boolean;

  constructor(http:Http){
    this.http=http;

  }
  // public baseUrl:string='http://localhost:8000/';
  // public baseUrl:string='http://192.168.10.105/Git%20repo/room-finder-web/public/';
  // public baseUrl:string='http://192.168.0.5/hybrid%20project/room-finder-web/public/';
  public baseUrl = "http://roomfinder.mountvisiontreks.com/";

}
