import {SingletonService} from './singleton.service';
import {Injectable} from '@angular/core'
import {Http, Headers} from "@angular/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import {App, ModalController, Platform} from "ionic-angular";
import {Storage} from '@ionic/storage';

@Injectable()

export class AppService {

  get city(): any {
    return this._city;
  }

  set city(value: any) {
    this._city = value;
  }

  get user_data(): any {
    return this._user_data;
  }

  set user_data(value: any) {
    this._user_data = value;
  }

  http: any;
  baseUrl: String;
  private _user_data: any;
  private _city: any;

  constructor(http: Http,
              public singletonService: SingletonService,
              private platform: Platform,
              private app: App,
              public storage: Storage,
              public modalCtrl: ModalController) {
    this.http = http;
    this.baseUrl = this.singletonService.baseUrl + 'api/';

    this.storage.get('user_data').then((val) => {
      this._user_data = val;
    });
    this.storage.get('city').then((val) => {
      this._city = val;
    });
  }

  userProperty() {
    return this.http.get(this.baseUrl + 'user/' + this._user_data.id + '/property').map(res => res.json())
  }

  getRecentProperties() {
    return this.http.get(this.baseUrl + 'get-recent-properties').map(res => res.json())
  }

  login(credentials) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl + 'auth/login', JSON.stringify(credentials), {headers: headers})
      .map(res => res.json());
  }

  registerUser(data) {
    console.log(JSON.stringify(data));
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl + 'auth/register', JSON.stringify(data), {headers: headers})
      .map(res => res.json());
  }

  submitProperty(data) {
    console.log(JSON.stringify(data));
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl + 'property/store/' + this._user_data.id, JSON.stringify(data), {headers: headers})
      .map(res => res.json());
  }

  updateProperty(id, data) {
    console.log(JSON.stringify(data));
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl + 'property/' + id + '/update', JSON.stringify(data), {headers: headers})
      .map(res => res.json());
  }

  updatePropertyLocation(id, data) {
    console.log(JSON.stringify(data));
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl + 'property/' + id + '/update/location', JSON.stringify(data), {headers: headers})
      .map(res => res.json());
  }

  getFilteredProperties(data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl + 'property/filter', JSON.stringify(data), {headers: headers})
      .map(res => res.json());
  }

  addFavoriteProperty(data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl + 'add-favorite', JSON.stringify(data), {headers: headers})
      .map(res => res.json());
  }

  getFavoriteProperty() {
    return this.http.get(this.baseUrl + 'user/'+ this.user_data.id+'/favorite')
      .map(res => res.json());
  }
}
