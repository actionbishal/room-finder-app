import { Injectable } from '@angular/core';
import {Http} from "@angular/http";

/*
  Generated class for the LocationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationsProvider {

  constructor(public http: Http) {
  }

  static haversineDistance(startLat, startLong, endLat, endLong) {
    let EARTH_RADIUS = 6371; // Approx Earth radius in KM

    let dLat = (endLat - startLat)* Math.PI / 180;
    let dLong = (endLong - startLong)* Math.PI / 180;

    startLat = (startLat)* Math.PI / 180;
    endLat = (endLat)* Math.PI / 180;

    let a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(startLat) * Math.cos(endLat) * Math.pow(Math.sin(dLong / 2), 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return EARTH_RADIUS * c; // <-- d
  }
}
